DROP TABLE IF EXISTS Departments CASCADE;
DROP TABLE IF EXISTS Employees CASCADE;
DROP TABLE IF EXISTS PhoneNumber CASCADE;

CREATE TABLE Departments (
    Code INTEGER PRIMARY KEY NOT NULL,
    Name TEXT NOT NULL UNIQUE,
    Budget REAL NOT NULL 
);

CREATE TABLE Employees (
    EmpId INTEGER PRIMARY KEY NOT NULL,
    Name TEXT NOT NULL ,
    LastName TEXT NOT NULL ,
    Department INTEGER NOT NULL , 
    CONSTRAINT fk_Departments_Code FOREIGN KEY(Department) 
    REFERENCES Departments(Code)
);

CREATE TABLE PhoneNumber (
    Number TEXT PRIMARY KEY NOT NULL,
    Employee INTEGER NOT NULL,
    CONSTRAINT fk_PhoneNumber FOREIGN KEY(Employee) 
    REFERENCES Employees(EmpId)
);

INSERT INTO Departments(Code,Name,Budget) VALUES(14,'IT',65000);
INSERT INTO Departments(Code,Name,Budget) VALUES(37,'Accounting',15000);
INSERT INTO Departments(Code,Name,Budget) VALUES(59,'Human Resources',240000);
INSERT INTO Departments(Code,Name,Budget) VALUES(77,'Research',55000);

INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 1,'Michael','Rogers',14);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 2,'Anand','Manikutty',14);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 3,'Carol','Smith',37);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 4,'Joe','Stevens',37);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 5,'Mary-Anne','Foster',14);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 6,'George','ODonnell',77);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 7,'John','Doe',59);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 8,'David','Smith',77);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES( 9,'Zacary','Efron',59);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES(10,'Eric','Goldsmith',59);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES(11,'Elizabeth','Doe',14);
INSERT INTO Employees(EmpId,Name,LastName,Department) VALUES(12,'Kumar','Swamy',14);

INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-000',1);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-001',1);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-002',1);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-003',2);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-004',2);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-005',2);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-006',4);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-007',5);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-008',5);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-009',5);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-010',7);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-020',8);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-030',9);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-040',10);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-050',10);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-060',10);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-070',11);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-080',11);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-090',11);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-100',11);
INSERT INTO PhoneNumber(Number,Employee) VALUES('01-000-200',11);
