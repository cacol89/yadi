DROP TABLE IF EXISTS Likes CASCADE;
DROP TABLE IF EXISTS Serves CASCADE;
DROP TABLE IF EXISTS Frequents CASCADE;

CREATE TABLE Likes (
    Drinker TEXT NOT NULL,
    Beer TEXT NOT NULL
);

CREATE TABLE Serves (
    Bar TEXT NOT NULL,
    Beer TEXT NOT NULL
);

CREATE TABLE Frequents (
    Drinker TEXT NOT NULL,
    Bar TEXT NOT NULL
);


INSERT INTO Likes(Drinker,Beer) VALUES('Bob','Budweiser');
INSERT INTO Likes(Drinker,Beer) VALUES('Bob','Heiniken');
INSERT INTO Likes(Drinker,Beer) VALUES('Joe','Heiniken');
INSERT INTO Likes(Drinker,Beer) VALUES('Jane','Labatt Blue');
INSERT INTO Likes(Drinker,Beer) VALUES('Jane','Molsen');
INSERT INTO Likes(Drinker,Beer) VALUES('Suzie','Miller');

INSERT INTO Serves(Bar,Beer) VALUES('IrishPub','Budweiser');
INSERT INTO Serves(Bar,Beer) VALUES('IrishPub','Heiniken');
INSERT INTO Serves(Bar,Beer) VALUES('IrishPub','Leffe');
INSERT INTO Serves(Bar,Beer) VALUES('TheBar','Budweiser');
INSERT INTO Serves(Bar,Beer) VALUES('TheBar','Miller');
INSERT INTO Serves(Bar,Beer) VALUES('DrinkPlace','Heiniken');

INSERT INTO Frequents(Drinker,Bar) VALUES('Bob','IrishPub');
INSERT INTO Frequents(Drinker,Bar) VALUES('Bob','TheBar');
INSERT INTO Frequents(Drinker,Bar) VALUES('Joe','DrinkPlace');
INSERT INTO Frequents(Drinker,Bar) VALUES('Jane','IrishPub');
INSERT INTO Frequents(Drinker,Bar) VALUES('Suzie','TheBar');
INSERT INTO Frequents(Drinker,Bar) VALUES('Suzie','DrinkPlace');
