DROP TABLE IF EXISTS Father CASCADE;
DROP TABLE IF EXISTS Mother CASCADE;

CREATE TABLE Father (
    Name TEXT NOT NULL,
    Child TEXT NOT NULL
);

CREATE TABLE Mother (
    Name TEXT NOT NULL,
    Child TEXT NOT NULL
);


INSERT INTO Father(Name,Child) VALUES('Homer','Bart');
INSERT INTO Father(Name,Child) VALUES('Homer','Lisa');
INSERT INTO Father(Name,Child) VALUES('Homer','Maggie');
INSERT INTO Father(Name,Child) VALUES('Abraham','Homer');
INSERT INTO Father(Name,Child) VALUES('Abraham','Herb');
INSERT INTO Father(Name,Child) VALUES('Clancy','Marge');
INSERT INTO Father(Name,Child) VALUES('Clancy','Patty');
INSERT INTO Father(Name,Child) VALUES('Clancy','Selma');

INSERT INTO Mother(Name,Child) VALUES('Marge','Bart');
INSERT INTO Mother(Name,Child) VALUES('Marge','Lisa');
INSERT INTO Mother(Name,Child) VALUES('Marge','Maggie');
INSERT INTO Mother(Name,Child) VALUES('Mona','Homer');
INSERT INTO Mother(Name,Child) VALUES('Mona','Herb');
INSERT INTO Mother(Name,Child) VALUES('Jackie','Marge');
INSERT INTO Mother(Name,Child) VALUES('Jackie','Patty');
INSERT INTO Mother(Name,Child) VALUES('Jackie','Selma');
