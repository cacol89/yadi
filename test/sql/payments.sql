DROP TABLE IF EXISTS Person CASCADE;
DROP TABLE IF EXISTS Service CASCADE;
DROP TABLE IF EXISTS Payment CASCADE;

CREATE TABLE Person (
    Id INTEGER PRIMARY KEY NOT NULL,
    Name TEXT NOT NULL
);

CREATE TABLE Service (
    Id INTEGER PRIMARY KEY NOT NULL,
    Description TEXT NOT NULL
);

CREATE TABLE Payment (
    PersonId INTEGER REFERENCES Person(Id),
    ServiceId INTEGER REFERENCES Service(Id),
    Amount FLOAT,
    Date TIMESTAMP
);

INSERT INTO Person(Id,Name) VALUES(1,'Harry');
INSERT INTO Person(Id,Name) VALUES(2,'Ron');
INSERT INTO Person(Id,Name) VALUES(3,'Hermione');
INSERT INTO Person(Id,Name) VALUES(4,'Ginny');

INSERT INTO Service(Id,Description) VALUES(1,'Daily Prophet subscription');
INSERT INTO Service(Id,Description) VALUES(2,'Wizard taxes');
INSERT INTO Service(Id,Description) VALUES(3,'Hogwarts tuition');

INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,1,23.45,'1998-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,1,20.50,'1999-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,1,30.10,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,2,123.43,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,2,432.23,'2001-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,2,1234.33,'2002-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,2,1234.2,'2003-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,3,3456.435,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(1,3,7456.435,'2010-10-15');

INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,1,3.45,'1998-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,1,2.50,'1999-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,1,3.10,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,1,0.10,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,2,143.43,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,2,4352.23,'2001-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,2,134.33,'2002-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,2,14.2,'2003-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,3,213436.435,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(2,3,432176.435,'2010-10-15');

INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(3,2,143.43,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(3,2,4352.23,'2001-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(3,3,3436.435,'2000-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(3,3,2176.435,'2010-10-15');

INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(4,1,10,'1998-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(4,1,11,'1999-10-15');
INSERT INTO Payment(PersonId,ServiceId,Amount,Date) VALUES(4,1,12,'2000-10-15');
