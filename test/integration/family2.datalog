%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 'Family' test suit.
% 
% This file contains tests over a simple family
% database that includes parental relations between the
% members of the family (mother and father). The
% file 'test/sql/family.sql' contains the DB schema and
% sample data for running the tests.
%
% The programs in this file are made to test mainly
% recursion in predicates with YADI.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%............................................
%............................................
%............................................
% All the test cases in this file contain
% sintactic/semantic errors: they test if
% yadi discovers and reports the errors.
%............................................
%............................................
%............................................

%%%%%%%%%%%%%%%%%
% Indirect recursion (not allowed)
%%%%%%%%%%%%%%%%%

Ancestor1(x,y) :- Father(x,y).
Ancestor1(x,y) :- Father(x,z), Ancestor2(z,y).
Ancestor2(x,y) :- Mother(x,y).
Ancestor2(x,y) :- Mother(x,z), Ancestor1(z,y).
Q(x,y) :- Ancestor1(x,y).
Q(x,y) :- Ancestor2(x,y).
?- Q(x,y).
/

P(x) :- Q(x).
Q(x) :- R(x), not P(x).
R(x) :- Mother(x,_).
?- P(x).
/

%%%%%%%%%%%%%%%%%%
%% Negative recursion (not allowed)
%%%%%%%%%%%%%%%%%%

Q(x) :- Father(x,_).
Q(x) :- Mother(x,_), not Q(x).
?- Q(x).
/

%%%%%%%%%%%%%%%%%%
% Multiple recursion (not allowed)
%%%%%%%%%%%%%%%%%%

Ancestor(x,y) :- Father(x,y).
Ancestor(x,y) :- Mother(x,y).
Ancestor(x,y) :- Father(x,z), Ancestor(z,y).
Ancestor(x,y) :- Mother(x,z), Ancestor(z,y).
?- Ancestor(x,y).
/

%%%%%%%%%%%%%%%%%%
% Double recursion (not allowed)
%%%%%%%%%%%%%%%%%%

Male_ancestor(x,y) :- Father(x,y).
Male_ancestor(x,y) :- Male_ancestor(x,z), Male_ancestor(z,y).
?- Male_ancestor(x,y).
/

%%%%%%%%%%%%%%%%%%
% No base case
%%%%%%%%%%%%%%%%%%

Q(x) :- Father(x,_), Q(x).
?- Q(x).
/

