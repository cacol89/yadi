#!/bin/bash

. test/scripts/parse_dbconf.sh

echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo "            YADI Automated tests               "
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo ""
echo "..............................................."
echo "             Integration Tests                 "
echo "..............................................."
echo ""

I=0
F=0
for tfile in `ls test/integration/*.datalog`
do
    I=$((I+1))

    CASE=`echo $tfile | awk -F/ '{print $NF}' | awk -F. '{NF--;print}'`
    printf "> Test #$I: $CASE... "
    IN="test/integration/$CASE.datalog"
    OUT="test/integration/$CASE.out"
    EXP="test/expected/integration/$CASE.out"
    yadi "$IN" "$OUT"
    
    DIFF=`diff $OUT $EXP`
    if [[ -z $DIFF ]]
    then
        echo "passed!"
    else
        echo "failed!"
        F_ARR[$F]=$CASE
        F=$((F+1))
    fi

    rm "$OUT"
done

printf '\n>>>>>>>>>>>>>>>>>\n\n'
if [[ $F -eq 0 ]]
then
    echo "All tests passed!"
else
    echo "$F out of $I tests failed!"
    echo ""
    echo "Failed cases:"
    echo ""
    for i in "${F_ARR[@]}"
    do
        echo "test/integration/$i.datalog"
    done
fi
