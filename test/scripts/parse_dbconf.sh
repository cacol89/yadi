#!/bin/bash

function print_error(){
    echo "$1" 1>&2
    exit 1
}

OLDIFS=$IFS
IFS="="
while read attr val
do
    if [ "$attr" = "host" ]
    then HOST=$val
    elif [ "$attr" = "port" ]
    then PORT=$val
    elif [ "$attr" = "user" ]
    then USER=$val
    elif [ "$attr" = "password" ]
    then PASSWORD=$val
    elif [ "$attr" = "dbname" ]
    then DBNAME=$val
    elif [ ! -z "$attr" ] || [ ! -z "$val" ]
    then
        print_error "Bad database config file: Parameter '$attr' not supported."
    fi
done
IFS=$OLDIFS

function yadi(){
    if [ ! -f $1 ]
    then
        print_error "Error, file $1 does not exist. Aborting."
    fi
    bin/yadi -h $HOST -p $PORT -U $USER -w $PASSWORD -d $DBNAME -f $1 > "$2" 2> /dev/null
}

