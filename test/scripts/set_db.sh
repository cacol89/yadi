#!/bin/bash

. test/scripts/parse_dbconf.sh

export PGPASSWORD=$PASSWORD

for i in `ls test/sql/*.sql`
do
    echo "...................................................."
    echo " Executing: $i"
    echo "...................................................."
    psql -h $HOST -p $PORT -U $USER -d $DBNAME < $i
done

unset PGPASSWORD
