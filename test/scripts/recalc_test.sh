#!/bin/bash

. test/scripts/parse_dbconf.sh < test/database.conf

if [[ $# -eq 1 ]]
then
    IN="test/integration/$1.datalog"
    OUT="test/expected/integration/$1.out"
    yadi "$IN" "$OUT"
    cat "$OUT"
else
    print_error "Only one argument must be provided: the name of the case (no directories!)"
fi
echo
