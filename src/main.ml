(*******************************************************)
(**  main.ml
 *
 *   Execution of YADI from command line, connection info
 *   help printouts for command line usage etc.
 * 
 *)
(********************************************************)

open Lexer;;
exception Eof;;
open Printf;;
open Postgresql;;
open Conn_ops;;
open Yadi_utils;;
open Arg;;

(** check for options of yadi command line*)
(*default values*)
let host = ref "localhost";;
let port = ref 5432;;
let user = ref "yadi";;
let password = ref "";;
let dbname = ref "yadi";;
let debug = ref false;;
let inputf = ref "";;

let usage = "usage: " ^ Sys.argv.(0) ^ " [OPTIONS]"
let speclist = [
    ("-h", Arg.String (fun s -> host := s),     "host     : database server host (default: \"yadi\")");
    ("-p", Arg.Int    (fun d -> port := d),     "port     : database server port (default: \"5432\")");
    ("-U", Arg.String (fun s -> user := s),     "user     : database user (default: \"yadi\")");
    ("-w", Arg.String (fun s -> password := s), "password : database user password (default: empty)");
    ("-d", Arg.String (fun s -> dbname := s),   "dbname   : database name to connect to (default: \"yadi\")");
    ("-db", Arg.Unit (fun () -> debug := true),  "        : print debugging information");
    ("-f", Arg.String (fun s -> inputf := s),   "file     : read program from file");
];;

let () =
  (* Read the arguments *)
  Arg.parse
    speclist
    (fun x ->
            raise (Arg.Bad ("Bad argument : " ^ x))
    )
    usage;
;;

(** assign postgreSQL connection parameters to conninfo variable *)
let conninfo =
    sprintf "host=%s port=%d user=%s password=%s dbname=%s"
    !host !port !user !password !dbname;;

(* pretty print connection informations *)
let print_conn_info conn =
    printf "dbname    = %s\n" conn#db;
    printf "user      = %s\n" conn#user;
    printf "password  = %s\n" conn#pass;
    printf "host      = %s\n" conn#host;
    printf "port      = %s\n" conn#port;
    printf "tty       = %s\n" conn#tty;
    printf "options   = %s\n" conn#options;
    printf "pid       = %i\n" conn#backend_pid
;;

(** everything is done here *)
let main () =
    let c = new connection ~conninfo () in
    if !debug then (
        print_conn_info c; 
        flush stdout
    ) else ();
    c#set_notice_processor (fun s -> eprintf "postgresql error [%s]\n" s);
    try 
        let chan = if !inputf = "" then stdin else open_in !inputf in
        let lexbuf = Lexing.from_channel chan in 
        let edb = import_dbschema c in 
        if !debug then (
            print_endline "--------------";
            print_endline "DB Schema:\n";
            print_symtable edb;
            print_endline "--------------";
            flush stdout;
        ) else ();
        while true do
            try
                print_string "\nyadi$ "; flush stdout;
                let ast = Parser.main Lexer.token lexbuf in 
                let sql = Eval.sql_stt (!debug) edb ast in
                if !debug then (
                    print_endline sql
                ) else ();
                print_sql_res c sql
            with
                  Yadi_error exp -> print_endline ("YADI execution error: "^exp)
                | Parsing.Parse_error -> print_endline "YADI parse error"
        done
    with Eof ->
        c#finish; exit 0
;; 

(** mainly a call to the above main function *)
let _ =
    try main () with
  | Error e -> prerr_endline (string_of_error e)
  | e -> prerr_endline (Printexc.to_string e)
;;
