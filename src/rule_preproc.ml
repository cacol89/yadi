(*******************************************************)
(**  rule_preproc.ml
 *
 *   Functions for preprocesing of IDB rules before
 *   SQL generation.
 *
 *)
(********************************************************)

open Expr;;
open Yadi_utils;;

(****************************************************)
(*      Recursion check functions                   *)
(****************************************************)

(** Returns true if the provided term is a call to the predicate described
 * by the provided key. If the recursive call is negative, an error is raised*)
let rec check_rec_term key = function
    | Rel rt ->
        let rt_key = symtkey_of_rterm rt in
        (key_comp key rt_key) = 0
    | Not rt ->
        if (check_rec_term key (Rel rt)) then
            raise (Yadi_error (
                "Predicate "^(string_of_symtkey key)^
                " is negatively recursive"
            ))
        else false
    | _ -> false

(** Returns true if the provided rule contains a call to itself.
 * If the recursive call is negative, then an error is raised.
 * If there is more than one recursive call to itself, an error is raised.*)
let is_rec_rule rule =
    let key = symtkey_of_rule rule in
    let body = rule_body rule in
    let rec_eval acc rule =
        if check_rec_term key rule then acc+1 else acc
    in
    let rec_count = List.fold_left rec_eval 0 body in
    if rec_count > 1 then
        raise (Yadi_error(
            "Predicate "^(string_of_symtkey key)^
            " has more than one recursive call"
        ))
    else
        rec_count = 1

(** Given a list of rules that define a predicate, checks that they
 * satisfy the recursion conditions of YADI, which are:
 *
 * - There cannot be negative recursive calls.
 * - A single rule cannot make more than one recursive call.
 * - There is at most one recursive rule.
 * - The predicate has a base-case.
 *
 * If the conditions are satisfied and there is a recursive rule,
 * then this recursive rule is placed at the end of the list.
 *)
let check_rec_rule rule_lst =
    let rec_split rule (r_lst,nr_lst) =
        if is_rec_rule rule then (rule::r_lst, nr_lst)
        else (r_lst, rule::nr_lst)
    in
    (*Split the rules in recursive and not revursive*)
    let (r_lst, nr_lst) = List.fold_right rec_split rule_lst ([],[]) in
    (*Check existence of base-case*)
    let key = symtkey_of_rule (List.hd rule_lst) in
    if nr_lst = [] then
        raise (Yadi_error(
            "Recursive predicate "^(string_of_symtkey key)^
            " does not contain a base case"
        ))
    else
    (*Check that there is no more than one recursive rule
     * and return list with recursion at the end*)
    match r_lst with
        | _::_::_ ->
            raise (Yadi_error (
                "Predicate "^(string_of_symtkey key)^
                " contains more than one recursive definition"
            ))
        | _ -> nr_lst@r_lst

(****************************************************)
(*      Constant extraction functions               *)
(****************************************************)

(** Extracts constants in a rterm and replaces them with
 * ad-hoc NumVars which numbers start from 'pos'. The function
 * returns a tuple with the new rterm and a list of equalities
 * that must be satisfied by the ad-hoc variables.
 *)
let extract_rterm_constants ?(pos = 0) rt = match rt with
    | Pred (x, vl) ->
        let extract var (v_lst,e_lst,i) = match var with 
            | ConstVar const -> 
                let nvar = NumberedVar i in
                let neq = Equal (nvar,const) in
                (nvar::v_lst, neq::e_lst,i+1) 
            | _ -> (var::v_lst, e_lst, i) in
        let (vars,eqs,_) = List.fold_right extract vl ([],[],pos) in 
        ( Pred (x,vars), eqs )

(** Given a rule, extracts all constants in its head and body rterms
 * for replacing them with ad-hoc variables placed in the body.
 * E.g. "Q(a,b,1) :- P(1,a,c) and R(b,3)." will be transformed to
 * "Q(a,b,_0) :- P(_1,a,c) and R(b_2) and _0 = 1 and _1 = 1 and _2 = 3."
 *)
let extract_rule_constants = function
    | Query _    -> invalid_arg "function extract_rule_constants called with a query"
    | Rule(h, b) -> 
        let (h2, h2_e) = extract_rterm_constants h in
        let extract t (t_lst,e_lst,i) = match t with
            | Rel rt -> 
                let (rt2,rt2_e) = extract_rterm_constants ~pos:i rt in
                ((Rel rt2)::t_lst, rt2_e@e_lst, i+(List.length rt2_e) )
            | _ -> (t::t_lst, e_lst, i) in
        let (b2,b2_e,_) = List.fold_right extract b ([],[],(List.length h2_e)) in
        Rule (h2,b2@h2_e@b2_e)
;;

(****************************************************)
(*      Main function                               *)
(****************************************************)

(** Given a symtable, performs preprocess operations over the rules
 * contained in it. This operations are:
 *  - Extraction of constants in predicates
 *  - Recursion checks (    only one recursive rule,
 *                          only one recursive call in rules,
 *                          no negated recursions,
 *                          and base case existance)
 *)
let preprocess_rules (st:symtable) =
    let rep_rule key rules =
        let rep_lst = List.map extract_rule_constants rules in
        let sorted_lst = check_rec_rule rep_lst in
        Hashtbl.replace st key sorted_lst in
    Hashtbl.iter rep_rule st
