 {
        open Parser        (* The type token is defined in parser.mli *)
(*		exception Eof
*)
 }

  rule token = parse
      [' ' '\t']     				{ token lexbuf }    (* skip blanks *)
    | ['\n' ]        				{ token lexbuf }    (* skip newline *)
    | '%'[^'\n']*'\n'        			{ token lexbuf }    (* skip comments *)
    | "yadi>"					{ token lexbuf }     (* skip prompt info *)
    | ['0'-'9']+ as lxm 			{ VAL(int_of_string lxm) }
    | '\''(('\'''\'')|[^'\n''\''])*'\'' as lxm  { STRING(lxm) }
    | ['A'-'Z']['a'-'z''0'-'9''_']* as lxm 	{ RELNAME(lxm) }
    | ['a'-'z']['a'-'z''0'-'9''_']* as lxm 	{ match lxm with
						    | "and" -> AND
						    | "not" -> NOT
						    | _     -> VARNAME(lxm)
						}
    | ":-"          				{ IMPLIEDBY }
    | "?-"            				{ QMARK }  (* query mark *)
    | '.'            				{ DOT }    (* end of rule or query *)
    | ','            				{ SEP }
    | '('            				{ LPAREN }
    | ')'            				{ RPAREN }
    | '='            				{ EQ }
    | "<>"            				{ NE }
    | '/'            				{ EOP }
    | '_'                                       { ANONVAR }
    | "<="                                      { LE }
    | ">="                                      { GE }
    | '<'                                       { LT }
    | '>'                                       { GT }
	
	
