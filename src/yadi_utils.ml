(*******************************************************)
(**  yadi_utils.ml
 *
 *   Yadi utils:
 *   This file contains data structure definitions and operations
 *   that are used along the program for different purposes.
 *   
 *)
(********************************************************)

open Expr;;

exception Yadi_error of string

(***********************************************************
 *  Symtable
 *********************************************************)

(** This type defines a symtable: a hash table that stores
 *  the set of rules that define a program. The keys
 *  are rule-name & rule-arity tuples and the values are lists of
 *  rules' AST specifications.
 *)
type symtkey = (string*int)
type symtable = (symtkey, stt list) Hashtbl.t

(** Prints a symtable
 *)
let print_symtable (st:symtable) =
    let print_el s = Printf.printf "%s" (string_of_stt s) in
    let print_lst _ lst = List.iter print_el lst in
    Hashtbl.iter print_lst st

(** Receives a rterm and generates its hash key for the
 *  symtable
 *)
let symtkey_of_rterm rt : symtkey = (get_rterm_predname rt, get_arity rt)

(** Receives a rule and generates its hash key for the
 *  symtable
 *)
let symtkey_of_rule rt : symtkey = match rt with
    | Rule (h, b) -> symtkey_of_rterm h
    | _ -> invalid_arg "function symtkey_of_rule called without a rule"

(** Inserts a rule in the symtable *)
let symt_insert (st:symtable) rule = match rule with
    | Rule (_,_) ->
        let key = symtkey_of_rule rule in
        if Hashtbl.mem st key then  
            Hashtbl.replace st key ((Hashtbl.find st key)@[rule])
        else
            Hashtbl.add st key [rule]
    | _ -> invalid_arg "function symt_insert called without a rule"

(** Compares two keys for ordering *)
let key_comp ((k1_n,k1_a):symtkey) ((k2_n,k2_a):symtkey) =
    let comp = String.compare k1_n k2_n in
    if comp != 0 then comp
    else k1_a - k2_a

(** Given a list of keys, remove repetitions *)
let remove_repeated_keys k_lst =
    let no_rep key = function
        | [] -> [key]
        | (hd::tl) ->
            if (key_comp key hd) == 0 then (hd::tl)
            else (key::hd::tl) in
    let sorted = List.sort key_comp k_lst in
    List.fold_right no_rep sorted []

(** Given a key, returns the predicate name that belongs to the key *)
let get_symtkey_predname ((n,_):symtkey) = n

(** Given a key, returns the predicate arity that belongs to the key *)
let get_symtkey_arity ((_,a):symtkey) = a

let string_of_symtkey ((n,a):symtkey) =
    n^"/"^(string_of_int a)

(**Takes a program and extracts all rules and places them in
* a symtable*)
let extract_idb = function
    | Prog stt_lst ->
        let idb:symtable = Hashtbl.create 100 in
        let in_stt t = match t with
            | Rule _ -> symt_insert idb t 
            | Query _ -> () in            
        List.iter in_stt stt_lst;
        idb

(***********************************************************
 *  kset (SymtkeySet)
 *********************************************************)

(**This structure defines a set of symtable keys*)
module SymtkeySet = Set.Make( 
  struct
    let compare = key_comp
    type t = symtkey
  end
)

type kset = SymtkeySet.t

(***********************************************************
 *  Colnamtab
 *********************************************************)

(** This type defines a colnamtab, which is a dictionnary that
 * contains for each predicate (edb & idb) a list with the name of
 * all of its columns in order. The information is stored in a
 * hash table using as a keys the keys from symtables.*)
type colnamtab = (symtkey, (string list)) Hashtbl.t

(*Extracts from the edb and idb their column names and
 * stores them in a colnamtab, places them in order*)
let build_colnamtab (edb:symtable) (idb:symtable) =
    let hs:colnamtab = Hashtbl.create 100 in
    let e_cols key rules =
        let rule = List.hd rules in
        let varlist = List.map string_of_var (get_rterm_varlist (rule_head rule)) in
        Hashtbl.add hs key varlist in
    Hashtbl.iter e_cols edb;
    let i_cols key rules =
        let rec cols ind n =
            if ind<n then ("col"^(string_of_int ind))::(cols (ind+1) n) 
            else [] in
        if not (Hashtbl.mem hs key) then
            Hashtbl.add hs key (cols 0 (get_symtkey_arity key))
        else
            ()
    in
    Hashtbl.iter i_cols idb;
    hs

(***********************************************************
 *  Vartab
 *********************************************************)

(** This type defines a 'vartable', it belongs to a rule and
 * it is a dictionary with variable names as key, these variables
 * are those that appear in the body/head of the rule.
 * The value for each key is a list of variable-appearances:
 * references to predicates in the rule's body where the variable is
 * mentioned.
 * A variable appearence is simply a string denoting
 * a column of a relation in the way Table.column*)
type vartab = (string, string list) Hashtbl.t

(*Inserts in a vartab the provided var_app, initializing a list
 * in the hash if neccessary*)
let vt_insert (vt:vartab) vname va =
    if Hashtbl.mem vt vname then
        let ap_lst = Hashtbl.find vt vname in
        Hashtbl.replace vt vname (va::ap_lst)
    else
        Hashtbl.add vt vname [va]

(*Prints a vartab*)
let vt_print (vt:vartab) =
    let print_el vn alst =
        let ap_str = "["^(String.concat ", " alst)^"]" in
        Printf.printf "%s: %s\n" vn ap_str in
    Hashtbl.iter print_el vt

(*builds a vartab out of a list of rterms and with the colnamtab*)
let build_vartab (col_names:colnamtab) rterms =
    let vt:vartab = Hashtbl.create 100 in
    let in_rt n rterm =
        let pname = get_rterm_predname rterm in
        let vlst = get_rterm_varlist rterm in
        let arity = get_arity rterm in
        let key = symtkey_of_rterm rterm in
        let cols = Hashtbl.find col_names key in
        let in_v cn v =
            let comp_cn =
                pname^"_a"^(string_of_int arity)^
                "_"^(string_of_int n)^"."^cn
            in
            match v with
            NamedVar _ | NumberedVar _ ->
                vt_insert vt (string_of_var v) comp_cn
            | AggVar _ -> raise (Yadi_error (
                    "Goal "^(string_of_symtkey key)^
                    " contains an aggregate function as a variable, "^
                    "which is only allowed in rule heads"
                ))
            | _ -> ()
        in
        List.iter2 in_v cols vlst;
        n+1
    in
    let _ = List.fold_left in_rt 0 rterms in
    vt

(***********************************************************
 *  Eqtab
 *********************************************************)

(** This type defines a eqtab, it belongs to a rule and it is
 * a dictionary with variable names as
 * keys and constants as values. They represent equalities that
 * must be satisfied by the variables*) 
type eqtab = (string,const) Hashtbl.t

(** Given a list of equality ASTs, returns an eqtab with
 * the equality relations as var = value.
 * PRECONDITION: There should not be aggregate equalities
 * in the provided list.*)
let build_eqtab eqs =
    let tuples = List.map extract_eq_tuple eqs in
    let hs:eqtab = Hashtbl.create 100 in
    let add_rel (var,c) = match var with
        NamedVar _ | NumberedVar _ -> Hashtbl.add hs (string_of_var var) c
        | _ -> invalid_arg "Trying to build_eqtab with equalities not of the form var = const" in
    List.iter add_rel tuples;
    hs

(** Given a var name, returns the value and removes it from the eqtab*)
let eqt_extract eqt vname =
    let c = Hashtbl.find eqt vname in
    Hashtbl.remove eqt vname;
    c

