(*******************************************************)
(**  stratification.ml
 *
 *   Functions for stratifying a datalog program.
 *   If any of the predicates is indirectly recursive,
 *   then the stratification will report an error and
 *   abort.
 *   The stratification will NOT check if a
 *   predicate is negatively self-recursive or if
 *   it has more than one recursive goals.
 *   The stratification will include only IDB relations
 *   and it will fail if the program is incomplete: if
 *   it contains references to undefined predicates.
 *
 *)
(********************************************************)

open Expr;;
open Yadi_utils;;

(** Checks that all predicates in rterms are in the edb/idb,
 * and returns those that contain idb predicates*)
let check_keys (edb:symtable) (idb:symtable) keys =
    let check key =
        if Hashtbl.mem edb key then false
        else if Hashtbl.mem idb key then true
        else raise ( Yadi_error 
            (   "Incomplete program, predicate "^
                (string_of_symtkey key)^" not defined"
            )
        )
    in
    List.filter check keys

(** Given a symtkey, returns the keys of all IDB predicates that
 * depend on it, positively or negatively.*)
let get_idb_graph_sons (edb:symtable) (idb:symtable) (key:symtkey) =
    let rule_lst = Hashtbl.find idb key in
    let rule_rts = List.map get_all_rule_rterms rule_lst in
    let rterms = List.flatten rule_rts in
    let keys = List.map symtkey_of_rterm rterms in
    let uniq_keys = remove_repeated_keys keys in
    check_keys edb idb uniq_keys

(** Performs a dfs in the dependency graph, if a cycle different
 * than a self-loop is found, an error will be raised.
 * Negative valued self-loops will not be checked.
 * The function will return a stratification of the program
 * from the called point.
 * The 'visit' and 'active' sets are used for keeping track of the
 * DFS explored nodes.*)
let rec strat_dfs edb idb visit active key =
    visit := SymtkeySet.add key (!visit);
    active := SymtkeySet.add key (!active);
    let sons = get_idb_graph_sons edb idb key in
    let rec_call son =
        if SymtkeySet.mem son (!visit) then
            if (key_comp son key) != 0 && SymtkeySet.mem son (!active) then
                raise (Yadi_error (
                    "Predicate "^(string_of_symtkey son)^
                    " is indirectly recursive."
                ))
            else []
        else
            strat_dfs edb idb visit active son
    in
    let strat = List.flatten (List.map rec_call sons) in
    active := SymtkeySet.remove key (!active);
    strat@[key]

(** Given the EDB and IDB of a program, and the rterm of an initial
 * query, calculates a stratification of the program. The stratification
 * is returned as a list of IDB symtkeys, where the first key is the
 * first IDB relation to calculate in the stratification.
 * If the provided program contains references to undefined predicates,
 * the function will raise an exception.
 * *)
let stratify (edb:symtable) (idb:symtable) (query_rt:rterm) : (symtkey list) =
    let visit = ref SymtkeySet.empty in
    let active = ref SymtkeySet.empty in
    let key = symtkey_of_rterm query_rt in
    (*Check that the queried relation exists*)
    let _ = check_keys edb idb [key] in
    (*If it is an EDB query, return an empty stratification*)
    if Hashtbl.mem edb key then
        []
    else
        strat_dfs edb idb visit active key
