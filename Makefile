OCAMLC_FLAGS=-I bin
PACKAGES=postgresql
DBCONF_FILE=test/database.conf

#Name of the final executable file to be generated
EX_NAME=yadi

#Name of the files that are part of the project
MAIN_FILE=main
FILES=\
    expr\
    parser\
    lexer\
    yadi_utils\
    rule_preproc\
    stratification\
    eval\
    conn_ops\

#The next variables hold the dependencies of each file
#DEP_example=dep1 dep2 dep3
DEP_expr=
DEP_parser=expr
DEP_lexer=parser
DEP_eval=expr yadi_utils stratification rule_preproc
DEP_conn_ops=expr yadi_utils
DEP_yadi_utils=expr
DEP_stratification=expr yadi_utils
DEP_query_flattening=expr
DEP_rule_preproc=expr yadi_utils

#Rule for generating the final executable file
bin/$(EX_NAME): $(FILES:%=bin/%.cmo) bin/$(MAIN_FILE).cmo
	ocamlfind ocamlc $(OCAMLC_FLAGS) -package $(PACKAGES) -thread -linkpkg $(FILES:%=bin/%.cmo) bin/$(MAIN_FILE).cmo -o bin/$(EX_NAME)

#Rule for compiling the main file
bin/$(MAIN_FILE).cmo: $(FILES:%=bin/%.cmo) src/$(MAIN_FILE).ml
	ocamlfind ocamlc $(OCAMLC_FLAGS) -package $(PACKAGES) -thread -o bin/$(MAIN_FILE) -c src/$(MAIN_FILE).ml 

#Special rule for compiling conn_ops
bin/conn_ops.cmo bin/conn_ops.cmi: src/conn_ops.ml
	ocamlfind ocamlc $(OCAMLC_FLAGS) -package $(PACKAGES) -thread -o bin/conn_ops -c $<

#Special rules for creating the lexer and parser
src/parser.ml src/parser.mli: src/parser.mly
	ocamlyacc $<
bin/parser.cmi:	src/parser.mli
	ocamlc $(OCAMLC_FLAGS) -o bin/parser -c $<
bin/parser.cmo:	src/parser.ml bin/parser.cmi
	ocamlc $(OCAMLC_FLAGS) -o bin/parser -c $<
src/lexer.ml:	src/lexer.mll
	ocamllex $<

#General rule for compiling
bin/%.cmi bin/%.cmo: src/%.ml
	ocamlc $(OCAMLC_FLAGS) -o bin/$* -c $<

#Dependencies:
#bin/example.cmo: $(DEP_example:%=bin/%.cmo)
bin/expr.cmo: $(DEP_expr:%=bin/%.cmo)
bin/parser.cmo: $(DEP_parser:%=bin/%.cmo)
bin/lexer.cmo: $(DEP_lexer:%=bin/%.cmo)
bin/eval.cmo: $(DEP_eval:%=bin/%.cmo)
bin/conn_ops.cmo: $(DEP_conn_ops:%=bin/%.cmo)
bin/yadi_utils.cmo: $(DEP_yadi_utils:%=bin/%.cmo)
bin/query_flattening.cmo: $(DEP_query_flattening:%=bin/%.cmo)
bin/stratification.cmo: $(DEP_stratification:%=bin/%.cmo)
bin/rule_preproc.cmo: $(DEP_rule_preproc:%=bin/%.cmo)

clean:
	rm -f bin/*.cmo bin/*.cmi src/parser.mli src/parser.ml src/lexer.ml 

tests:
	test/scripts/run_tests.sh < $(DBCONF_FILE)
testdb:
	test/scripts/set_db.sh < $(DBCONF_FILE)
