\documentclass[a4paper,10pt]{article}
\usepackage{amsmath}
\usepackage[a4paper]{geometry}

\title{EM-DMKM. Advanced Databases.\\ YADI: Yet Another Datalog Interpreter.\\User Manual.}
\author{
  Carlos A. Colmenares R.
}
\date{January 27th, 2013}

\begin{document}
\maketitle

This document contains a brief description about how to install and use YADI.

\section{Dependencies}

In order to compile this software, it is necessary to have the following software installed and ready
to use in the machine:

\begin{itemize}
    \item ocaml $(\ge 4.0.0)$
    \item PostgreSQL $(\ge 9.0)$
    \item postgresql-ocaml $(\ge 2.0.1)$
    \item findlib $(\ge 1.3.1)$
    \item GNU Make $(\ge 3.81)$
\end{itemize}

\section{Pre-requisites}

For running YADI it is necessary to have access to a PostgreSQL database;
when starting the program it will be necessary to provide a username, database name, host, port and password.
Obviously the provided user will need to have write/read permission over the provided database.

\section{Installation}

For installing YADI follow these steps:

\begin{enumerate}
    \item Uncompress the source archive and go to the root directory.
    \item From the root directory, run ``make''.
    \item The final executable file can be found in the ``bin/'' directory and it will be called ``yadi'', i.e: \emph{bin/yadi}.
    \item For starting to use the program, just execute the \emph{yadi} file, for information about the
        command line, pass the ``--help'' flag.
\end{enumerate}

\section{Invoking the interpreter}

The usage of the generated executable file is the following one:

\begin{verbatim}
./yadi [OPTIONS]

OPTIONS:
  -h host     : database server host (default: "yadi")
  -p port     : database server port (default: "5432")
  -U user     : database user (default: "yadi")
  -w password : database user password (default: empty)
  -d dbname   : database name to connect to (default: "yadi")
  -db         : print debugging information
  -f file     : read program from file
  -help  Display this list of options
  --help  Display this list of options
\end{verbatim}

Where the provided arguments correspond to the needed information to access the PostgreSQL database. Please note that there
are default values to the arguments. The following line contains an example invocation of the executable:

\begin{verbatim}
    ./yadi -h 127.0.0.1 -p 5432 -U myuser -w secret -d mydb
\end{verbatim}

If the connection was correctly done, then the following line should show:

\begin{verbatim}
yadi$ 
\end{verbatim}

Which corresponds to the program's prompt.

Note that if the flag ``-db'' is passed, YADI will print debugging information. For example the following list can get printed:

\begin{verbatim}
dbname    = mydb
user      = myuser
password  = secret
host      = localhost
port      = 5432
tty       = 
options   = 
pid       = 7200
--------------
DB Schema:

Person(id,name) :- .
Likes(drinker,beer) :- .
Serves(bar,beer) :- .
Mother(name,child) :- .
Payment(personid,serviceid,amount,date) :- .
Service(id,description) :- .
Departments(code,name,budget) :- .
Father(name,child) :- .
Frequents(drinker,bar) :- .
Phonenumber(number,employee) :- .
Employees(empid,name,lastname,department) :- .
--------------

yadi$ 
\end{verbatim}

The first set of information is related to the DB connection. The second set of information is related to the schema of the underlying database,
or in other words, the EDB relation. For instance, the line:

\begin{verbatim}
Serves(bar,beer) :- .
\end{verbatim} 

Means that there is a EDB relation named ``Served'' that maps to a table with two columns: the first one named ``bar'' and the second one named ``beer''.

\section{Running queries}

Datalog programs can be directly placed in the YADI prompt or can be read from a file when providing the ``-f'' flag. YADI will not
parse the program until the character ``/'' is found. The following lines contain an example of a program:

\begin{verbatim}
P(x) :- Father(x,_).
?- P(x).
/
\end{verbatim}

In general, predicate names start with capital letters, and variables with lower-case letters. A program must have one, and exactly one,
query. It is also possible to directly query EDB relations, for instance:

\begin{verbatim}
?- Father(x,y).
/
\end{verbatim}

It is important to highlight that constants can appear in queries but not anonymous variables.

If the flag ``-db'' is passed to the argument line, then the stratification and generated SQL of the program will be printed. For example,
the program:

\begin{verbatim}
F(x) :- Father(x,_).
M(x) :- Mother(x,_).
G(x) :- F(x).
G(x) :- M(x).
?- G(x).
/
\end{verbatim}

Generates this output:

\begin{verbatim}
______________
Stratification:

F/1
M/1
G/1
______________
WITH RECURSIVE F_a1 AS (
    SELECT Father_a2_0.name AS col0
    FROM Father AS Father_a2_0
), M_a1 AS (
    SELECT Mother_a2_0.name AS col0
    FROM Mother AS Mother_a2_0
), G_a1 AS (
    SELECT F_a1_0.col0 AS col0
    FROM F_a1 AS F_a1_0
    UNION ALL
    SELECT M_a1_0.col0 AS col0
    FROM M_a1 AS M_a1_0
)
SELECT G_a1_0.col0 AS col0
FROM G_a1 AS G_a1_0  ;

-----------
| col0    |
-----------
| Homer   |
| Homer   |
| Homer   |
| Abraham |
| Abraham |
| Clancy  |
| Clancy  |
| Clancy  |
| Marge   |
| Marge   |
| Marge   |
| Mona    |
| Mona    |
| Jackie  |
| Jackie  |
| Jackie  |
-----------
\end{verbatim}

The first set of information is the stratification of the program, it means that predicate ``F/1'' will be calculated first, then the predicate ``M/1'' and so on. The number
after the slash represents the arity of the predicate.\\

For sample programs, see the files inside directory ``test/integration''.\\

Please refer to the project's report
for information about the expressive power and internal functionality of YADI.

\section{Running tests}

YADI includes a test database and scripts for automatically setting it and performing tests. For using this feature it is
necessary to fill the file ``test/database.conf'' with the information about the database connection to use as test database. An example
of the file filled with the required information is the following one:

\begin{verbatim}
host=localhost
port=5432
user=myuser
password=secret
dbname=mydb
\end{verbatim}
 
Then, for automatically setting the test database just go to the project's root directory and execute ``make testdb''. This task will basically
pass all the files inside the ``test/sql'' directory to PostgreSQL.

For performing the automatic execution of tests, first make sure the test database is set and then execute inside the project's root directory
``make tests''. An output similar to the next one must be generated:

\begin{verbatim}
HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
YADI Automated tests               
HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

...............................................
 Integration Tests                 
 ...............................................

 > Test #1: beer... passed!
 > Test #2: employees2... passed!
 > Test #3: employees... passed!
 > Test #4: family2... passed!
 > Test #5: family... passed!
 > Test #6: payment2... passed!
 > Test #7: payment... passed!

 >>>>>>>>>>>>>>>>>

 All tests passed!
\end{verbatim}

If any test failed, then it will be reported.

What this procedure does is to execute with YADI all ``.datalog'' files inside the ``test/integration'' directory, and compare the output
to the corresponding file in ``test/expected/integration'' directory.

\section{Uninstalling}

For uninstalling YADI, simply erase its root directory.

\end{document}
