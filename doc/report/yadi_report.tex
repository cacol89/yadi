\documentclass[a4paper,10pt]{report}
\usepackage[a4paper]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{hyperref}

\title{EM-DMKM. Advanced Databases.\\ YADI: Yet Another Datalog Interpreter }
\author{
  Carlos A. Colmenares R.
}
\date{December 27, 2013}

\begin{document}
\maketitle
\tableofcontents

\chapter{Project definition}

Yet Another Datalog Interpreter (YADI) is a project for converting Datalog queries
into SQL statements. 

Datalog is a declarative logic programming language. It will be used as a query language
in this project. For conversion into SQL, first we need to create Abstract Syntax Tree,
and provide an SQL statement. At the end, from SQL statements, the project tries to
produce the answer set of tuples.\\

A Datalog query for \{R(A,B), S(B,C,D)\} database looks like this:

\begin{verbatim}
V(x,y) :- R(x,y) and S(y,_,_).
Q(x,y) :- S(x,y,z) and V(z,t) and t>=3.
?- Q(x,y).
\end{verbatim} 

The example above states that we have some \textit{predicates} and \textit{query} in
the first two lines. And the third line is the actual query. Even though the predicates
above states a Datalog query with multiple-rules, since it's a \textit{Conjunctive Query}
so that we can translate that into SQL statement.

\begin{verbatim}
SELECT B, C
FROM R INNER JOIN S S1 USING (B)
INNER JOIN S S2 ON (R.A=S2.D)
WHERE S1.B>=3; 
\end{verbatim} 

The purpose of the project is to develop a Command Line Interpreter for Datalog queries.
It aims at converting Datalog queries into SQL statements. Then, a backend Relational
Database is in charge of evaluating the query. 

\chapter{Current state of the project}

The project was implemented as requested and several levels of complexity were
reached, the following sections describe in detail the features included in
the project's current state as well as the kind of Datalog programs it accepts.

For a list of known issues and features yet to implement, please refer to
chapter \ref{chap:issues}.

\section{Expressive power}\label{sec:expressive_power}

In its current state, YADI is able to solve Datalog programs with conjunctive queries,
negation and recursion. Furthermore, YADI applies stratification for solving the programs
(refer to section \ref{sec:stratification} for more detail).

Nevertheless, because of SQL limitations, YADI is not able to solve any kind of recursive
programs; it only accepts recursive Datalog programs that satisfy the five following conditions:

\begin{enumerate}
    \item There can be no mutually recursive predicates. In other words, the only accepted
        form of recursion is self-recursion. For instance, the following program is not
        accepted by YADI since the predicates P/1 and Q/1 are mutually recursive.
        \begin{verbatim}
            P(x) :- Q(x), R(x).
            Q(x) :- P(x), S(x).
            ?- Q(x).
        \end{verbatim}

    \item A predicate definition cannot include itself as a negative goal. In other words, negative
        recursion is not allowed. For instance, the following program is not accepted by YADI
        since the second definition of predicate P/2 contains a negated recursive goal.
        \begin{verbatim}
            P(x,y) :- R(x,y).
            P(x,y) :- R(y,x), not P(x,y).
            ?- P(x,y).
        \end{verbatim}
    
    \item A predicate definition cannot contain more than one recursive goal in its body. In other words,
        only one recursive call is allowed in predicate definitions. For instance, the following program
        is not accepted by YADI since the second definition of predicate T/2 contains two recursive goals. 
        \begin{verbatim}
            T(x,y) :- G(x,y).
            T(x,y) :- T(x,z), T(z,y).
            ?- T(x,y).
        \end{verbatim}
    
    \item Predicates can have at most one recursive definition. For instance, the following program is not
        accepted by YADI since the predicate T/2 contains two recursive definitions.
        \begin{verbatim}
            T(x,y) :- G(x,y).
            T(x,y) :- G(x,z), T(z,y).
            T(x,y) :- T(y,x).
            ?- T(x,y).
        \end{verbatim}

    \item Every predicate must have a non-recursive definition. In other words, every recursive predicate must
        include a base-case definition. For instance, the following program is not accepted by YADI since
        predicate T/2 only contains a recursive definition.
        \begin{verbatim}
            T(x,y) :- G(x,z), T(z,y).
            ?- T(x,y).
        \end{verbatim}
\end{enumerate}

Due to these conditions, the language accepted by YADI is more expressive than $\text{nonrecursive-Datalog}^{\lnot}$ but
less expressive than $\text{stratified-Datalog}^{\lnot}$ because it does not accept, for example, mutual recursion. Formally:

$$\text{nonrecursive-Datalog}^{\lnot} \subset \text{YADI-Datalog}^{\lnot} \subset \text{stratified-Datalog}^{\lnot}$$

\subsection{Safety analysis}

YADI only accepts ``safe programs'', where a program is considered safe if the following
conditions are met:

\begin{itemize}
    \item For any rule, all variables inside its head appear in positive goals or strict equality relations in its body. 
        For example, the following program is not considered safe because variables ``z'' and ``v'' do not
        appear in any positive goal nor a strict equality (like variable ``y''):
        \begin{verbatim}
            P(x,y,z,v) :- R(x), y = 5, z < 10.
            ?- P(x,y,z,v).
        \end{verbatim}
    \item For any rule, all variables inside negated goals must appear in positive goals or strict equalities.
        For example, the following program is not considered safe because variable ``y'' does not
        appear in any positive goal nor a strict equality:
        \begin{verbatim}
            P(x) :- R(x), y<=100, not R(x,y).
            ?- P(x).
        \end{verbatim}
\end{itemize}

\section{User interface}

YADI includes a rather simple user interface for performing queries. When executed, the program
allows to provide database connection information through flags (refer to the user manual for further information),
as well as a path for a script-file to read and execute. If the flag ``-db'' is provided, then debugging information
will be printed, which includes database information, stratification of the program, parsing information, generated SQL and more.

Finally, once YADI has been executed a command-line interpreter will be shown, which allows
to write Datalog programs and see the resulting tables, or know if any semantic/syntactic errors have been found
during execution.

\section{Datalog-like syntax}

As stated before, the language accepted by YADI dominates $\text{nonrecursive-Datalog}^{\lnot}$
and includes parts of $\text{stratified-Datalog}^{\lnot}$. Besides it also accepts anonymous variables, constants inside
predicates, and aggregate functions. The following list summarizes this features accepted by the project's
current state:

\begin{itemize}
    \item Any $\text{nonrecursive-Datalog}^{\lnot}$ program is accepted by YADI.
    \item Inequality relations between variables and constants are also accepted.
        The constants can be integers, strings or real numbers.
        For example, the following program is accepted by YADI:
        \begin{verbatim}
            P(x,y,z) :- R(x), S(y), T(z), x <= 10, y <> 5, z = 'something'.
            ?- P(x,y,z).
        \end{verbatim}
        Where the operator ``$<>$'' stands for not equal ($\neq$). 
    \item Programs with positively recursive predicates are accepted. Refer to section \ref{sec:expressive_power}
        for more details.
    \item Constants can appear inside queries, rule heads and goals.
        For example, the following program is accepted by YADI:
        \begin{verbatim}
            P(x,y,23) :- R(x,1), S(23.54,x), T(y,'string').
            ?- P('example',12,z).
        \end{verbatim}
    \item Anonymous variables can appear in goals of rules, but not inside queries or
        predicate definitions.
    \item Aggregate functions are accepted in predicate definitions. The accepted functions
        are MAX, MIN, SUM, COUNT, and SUM. The aggregation will apply to the grouping of
        non-aggregated variables inside the rule's definition, please refer to section \ref{sec:aggregation}
        for more information.
        For example, the following program is accepted by YADI:
        \begin{verbatim}
            P(x,count(y)) :- R(x,y).
            ?- P(x,cy).
        \end{verbatim}
    \item Equality and inequality conditions between aggregated variables and constants are also accepted.
        However, if the aggregate is not defined in the rule's head, it cannot be used for comparisons
        in the body.
        For example, the following program is accepted by YADI:
        \begin{verbatim}
            P(x,sum(y)) :- R(x,y), sum(y) < 1000.
            ?- P(x,sy).
        \end{verbatim}
        Nevertheless, the following one is not since the aggregate ``sum(y)'' is not defined in the rule's head:
        \begin{verbatim}
            P(x,y) :- R(x,y), sum(y) < 1000.
            ?- P(x,y).
        \end{verbatim}
\end{itemize}

\section{Automated tests}

The project includes scripts and files that allow to automatically set a test database and perform automated tests.
There are four test suits included, where each one tests different specific features of YADI:

\begin{itemize}
    \item \textbf{Employee test suit}: tests conjunctive query features, but without recursion or negation.
    \item \textbf{Beer test suit}: tests negation features.
    \item \textbf{Family test suit}: tests recursion features.
    \item \textbf{Payment test suit}: tests aggregation features.
\end{itemize}

Please refer to the user manual for information about how to run the automated tests.

\chapter{Project's structure}

The project's root contains several files and directories. The \emph{bin/} directory holds binaries generated during compilation, and
it will hold notably the final executable called ``yadi''. The \emph{doc/} directory contains the latex source for this report and
the user manual. The \emph{lib/} directory is meant to contain special libraries needed for the project, nevertheless since YADI does not need
special libraries this directory is currently empty. Finally, the most important directories are the \emph{src/} and \emph{test/} ones,
which description will be made in the following sections.

\section{The \emph{src/} directory}

This directory contains the project's source code, it holds the following files:

\begin{itemize}
    \item \textbf{conn\_ops.ml}: contains all the logic for connecting to the database, perform SQL queries, and fetching/printing the result.
    \item \textbf{eval.ml}: contains the logic for transforming ASTs into SQL.
    \item \textbf{expr.ml}: contains the data model for ASTs and functions for accessing/transforming the data inside the trees.
    \item \textbf{lexer.mll}: contains the description of the tokens used for parsing programs.
    \item \textbf{main.ml}: contains the logic for the command-line interpreter and general user interface.
    \item \textbf{parser.mly}: contains the grammar definition for the Datalog language accepted by YADI.
    \item \textbf{rule\_preproc.ml}: contains logic for performing preprocessing of rules before SQL generation,
        such as safety checks and optimizations.
    \item \textbf{stratification.ml}: contains the logic for performing the stratification of a Datalog program.
    \item \textbf{yadi\_utils.ml}: contains the data model of the main data structures used along the program as well as functions
        for accessing/operating the structures.
\end{itemize}

\section{The \emph{test/} directory}

This directory contains all the files involved in the testing of the software. It includes the following sub-directories
and files:

\begin{itemize}
    \item \textbf{database.conf}: This file should be filled by the user with information about the database to use for tests.
    \item \textbf{expected/}: This directory contains the expected results of the tests inside the \emph{integration} directory. A test
        is passed if the result is equal to the expected one.
    \item \textbf{integration/}: The directory contains all the integration tests to be performed. The automated tests consist in running YADI with
        all the files in this directory and compare the obtained results with the expected ones, which lie in the \emph{expected} directory.
    \item \textbf{scripts/}: This directory contains all the shell-scripts that contain the logic for performing automated tests and
        setting of the test database.
    \item \textbf{sql/}: This directory contains the SQL scripts for setting the test database that is necessary for running the tests in the
        \emph{integration} directory. When automatically setting the test database, all SQL files in this directory will be provided to
        the DBMS.
\end{itemize}

\chapter{SQL generation}

This chapter contains a brief explanation of the strategy used for transforming Datalog programs into SQL.

\section{Conjunctive queries}

The general process of generating SQL out of simple conjunctive queries was divided in three
parts: SELECT-clause generation, FROM-clause generation, and WHERE-clause generation. In this part
all predicates involved in rule definitions are assumed to belong to the EDB, this assumption is correct
because YADI stratifies programs before solving them, which will be explained in section \ref{sec:stratification}.

\subsection{SELECT-clause generation}

The select clause is composed exclusively of the variables that appear in the rule's head, however,
these variables had to be mapped to EDB-table column-names or constants. For doing this, an appearance of the variable
in the rule's body is searched; if it is found in a predicate then the variable is mapped to the corresponding column-name
of the predicate, else, if it is found in a strict equality it takes the value of the constant.
Finally, the columns in the resulting table are aliased with the names ``col0'', ``col1'',
and so on.

For instance, the rule:

\begin{verbatim}
P(x,y,1) :- R(x), y = 'string'.
\end{verbatim}

Would generate the following SELECT-clause:

\begin{verbatim}
SELECT R_a1_0.first_col_name as col0, 'string' as col1, 1 as col2
\end{verbatim}

The postfix ``\_a1\_0'' added to table-name R will be explained further on.

\subsection{FROM-clause generation}

The FROM-clause is composed of the table-names of all the positive predicates that are in the rule's body.
Since a same predicate can appear more than once in a rule's body, the table names are aliased with a postfix ``\_X'' (where
X is the position of the goal in the rule) so as to let SQL differentiate between the different appearances. For example, the rule:

\begin{verbatim}
P(x,y) :- R(x), R(y), S(y,_).
\end{verbatim}

Would generate the following FROM-clause:

\begin{verbatim}
FROM R_a1 as R_a1_0, R_a1 AS R_a1_1, S_a2 AS S_a2_2
\end{verbatim}

The postfix ``\_aN'' added to table-names is related to arity and will be explained further on.

\subsection{WHERE-clause generation}

If a variable appears more than once in positive goals of a rule, then a JOIN between these tables
is performed in the WHERE clause. Also all variable comparisons are included in this clause.
For doing this task, variables are mapped to EDB-column names in the same way it is done for the
SELECT-clause. The aliases given to the tables in the FROM clause are taken into account.

For example, the following rule:

\begin{verbatim}
P(x,y) :- R(x), S(x,y), x < 5, y = 4.
\end{verbatim}

Would generate the following WHERE-clause:

\begin{verbatim}
WHERE R_a1_0.col0 = S_a2_1.col0 AND R_a1_0.col0 < 5 AND S_a2_1 = 4
\end{verbatim}

\section{Union}

When a predicate has several definitions, then the SQL of each definition is generated separately and combined
with ``UNION ALL'' statements. 
For example, the following rules:

\begin{verbatim}
P(x) :- S(x). 
P(x) :- G(x). 
\end{verbatim}

Would generate the following SQL:

\begin{verbatim}
SELECT S_a1_0.col0 AS col0 FROM S_a1 AS S_a1_0
UNION ALL
SELECT G_a1_0.col0 AS col0 FROM G_a1 AS G_a1_0
\end{verbatim}

\section{Stratification}\label{sec:stratification}

Before SQL generation YADI performs a stratification of the Datalog program, this stratification is
basically a topological sort of the rules. YADI will build a dependency graph between the
predicates where predicate B will depend on predicate A if predicate B appears as a positive or negative
goal in some definition of A. Once the stratification is made, all the predicate's tables are calculated
sequentially using WITH statements, and finally the final query is performed.
For example, the following program:

\begin{verbatim}
Q(x) :- M(x). 
Q(x) :- F(x). 
M(x) :- Mother(x,_).
F(x) :- Father(x,_).
?- Q(x).
\end{verbatim}

Would produce the following stratification: $(\{F/1\}, \{M/1\}, \{Q/1\})$, and the following SQL:

\begin{verbatim}
WITH RECURSIVE F_a1 AS (
    SELECT Father_a2_0.name AS col0
    FROM Father AS Father_a2_0
), M_a1 AS (
    SELECT Mother_a2_0.name AS col0
    FROM Mother AS Mother_a2_0
), Q_a1 AS (
    SELECT M_a1_0.col0 AS col0
    FROM M_a1 AS M_a1_0
    UNION ALL
    SELECT F_a1_0.col0 AS col0
    FROM F_a1 AS F_a1_0
)
SELECT Q_a1_0.col0 AS col0
FROM Q_a1 AS Q_a1_0  ;
\end{verbatim}

It is important to highlight that every IDB predicate P is calculated in a table named ``P\_aX'', where X is the arity of the predicate. This is done so as to allow
definitions of predicates with the same name but different arity.

\section{Recursion}

Due to stratification, recursion is trivially solved by including the word "RECURSIVE" in the WITH-statement generated
when calculating IDB-tables. However, SQL asks to place recursive calls only after base-cases and with ``UNION ALL''
connectors, therefore every rule is preprocessed and if it contains a recursive definition it is placed at the bottom
of the list. For instance, the program:

\begin{verbatim}
Ancestor(x,y) :- Father(x,z), Ancestor(z,y).
Ancestor(x,y) :- Father(x,y). 
?- Ancestor(x,y)./
\end{verbatim}

Will be rearranged to:

\begin{verbatim}
Ancestor(x,y) :- Father(x,y). 
Ancestor(x,y) :- Father(x,z), Ancestor(z,y).
?- Ancestor(x,y)./
\end{verbatim}

And will generate the following SQL:

\begin{verbatim}
WITH RECURSIVE Ancestor_a2 AS (
    SELECT Father_a2_0.name AS col0, Father_a2_0.child AS col1
    FROM Father AS Father_a2_0
    UNION ALL
    SELECT Father_a2_0.name AS col0, Ancestor_a2_1.col1 AS col1
    FROM Father AS Father_a2_0, Ancestor_a2 AS Ancestor_a2_1
    WHERE Ancestor_a2_1.col0 = Father_a2_0.child
)
SELECT Ancestor_a2_0.col0 AS col0, Ancestor_a2_0.col1 AS col1
FROM Ancestor_a2 AS Ancestor_a2_0  ;
\end{verbatim}

\section{Negation}

Negation is handled with sub-queries in the WHERE clause, using NOT EXISTS statements.
The inner WHERE-clause of this sub-query will be comprised of equalities between columns
of the negated table and columns of non-negated tables or constants. The process of mapping
variables to EDB-table column-names is similar as in the generation of normal WHERE-clauses.
For example,
the following rule:

\begin{verbatim}
P(x) :- R(x), y=1, not S(x,y).
\end{verbatim}

Will generate the following SQL:

\begin{verbatim}
SELECT R_a1_0.col0 AS col0
FROM R_a1 AS R_a1_0
WHERE NOT EXISTS (
    SELECT *
    FROM S_a2
    WHERE col0 = R_a1_0.col0 AND col1 = 1
)
\end{verbatim}

\section{Aggregation}\label{sec:aggregation}

Aggregation is split in two phases: GROUP-BY-clause generation and HAVING-clause generation.

\subsection{GROUP-BY-clause generation}

When there are aggregate functions in a rule's head, it is assumed that the grouping is performed over the
non-aggregated non-constant variables of the rule's head. However, if there are no such variables, then
there will be no GROUP-BY-clause.

For example, the following rule:

\begin{verbatim}
Q(pid,sum(p),42) :- Payment(pid,_,p,_).
\end{verbatim}

Only has a single non-aggregated, non-constant variable in its head (which correspond to the first column),
hence the generated GROUP-BY clause would be:

\begin{verbatim}
GROUP BY col0
\end{verbatim}

Please refer to the following sub-section for a complete query example.

\subsection{HAVING-clause generation}

YADI allows to make comparisons of aggregated functions in a rules body, under the condition that the aggregated
variable involved in the comparison must appear in the rule's head. This comparisons are placed in
the HAVING-clause of the generated SQL.
For example, the following query:

\begin{verbatim}
Q(pid,sum(p)) :- Payment(pid,_,p,_), sum(p) > 12000.
?-Q(pid,sp).
\end{verbatim}

Generates the following SQL:

\begin{verbatim}
WITH RECURSIVE Q_a2 AS (
    SELECT Payment_a4_0.personid AS col0, SUM(Payment_a4_0.amount) AS col1
    FROM Payment AS Payment_a4_0
    GROUP BY col0
    HAVING SUM(Payment_a4_0.amount) > 12000
)
SELECT Q_a2_0.col0 AS col0, Q_a2_0.col1 AS col1, Q_a2_0.col2 AS col2
FROM Q_a2 AS Q_a2_0  ;
\end{verbatim}

\chapter{Project management}

This chapter contains information about the tools used for managing and maintaining the porject.

\section{Revision control system}

The site Bitbucket (\url{http://www.bitbucket.org}) was used as online repository for hosting the project and allowing
version control of it. It was chosen mainly because it allows private repositories and compatibility with git (\url{http://git-scm.com}),
which was the version control system chosen for the project.

The project's repository will be made public in the future and will be found on the link \url{https://bitbucket.org/cacol89/yadi}.

\section{Software development process}

For organizing the team for developing the software, it was agreed to use a methodology similar to SCRUM. The main characteristics
where the following ones:

\begin{itemize}
    \item The work to be done was grouped and split in three deliveries, each one comprised one month of work.
    \item Weekly meetings were agreed where each member would state in what he has been working, the problems
        he has found so far, and what he intends to do during the week.
    \item At the end of each delivery, a meeting was performed to analyze the work done, what was missing, and
        probably change or re-organize the tasks to be done for the following delivery.
\end{itemize}

\section{Task management}

For assigning work to project members and keep track of what everyone had done, the site Trello (\url{https://trello.com/}) was used.
There were several boards that kept track of the features to be implemented, being implemented, and finished, as well as boards
for reporting spotted bugs and ideas for future features.

\chapter{Known issues and future work}\label{chap:issues}

Although YADI performs several checks, there are some of them that could not be implemented for time constraints. They are mainly
the following ones:

\begin{itemize}
    \item \textbf{SQL injection}: YADI accepts strings as constants, however, this strings are not checked for avoiding SQL injection.
    \item \textbf{Type checks}: YADI does not check the type of columns in EDB or IDB relations. Therefore, if asked to, it
        can try to make comparisons between, for instance, an integer-typed column and a string constant, which will result in
        a SQL error. For fixing this problem it would be necessary to perform type-inferences in the columns of IDB rules,
        for then being able to perform type-checks between variables and constants.
\end{itemize}

The following list contains features that should be implemented for future releases of YADI:

\begin{itemize}
    \item \textbf{Variable comparisons}: YADI only accepts comparisons between variables and constants, but not
        between two variables (e.g. ``x $\le$ y'', ``z = v'', etc.). The main difficulty that this entails is related
        to the fact that SQL does not allow to declare variables, and therefore YADI would have to analyze and
        ``flatten'' comparison chains. For example the program:
        \begin{verbatim}
            P(a,g) :- R(a,b), b<=c, c<=d, d<=e, e=f, f=g, S(g).
            ?- P(x,y)
        \end{verbatim}
        Will have to be transformed to the next one before SQL generation:
        \begin{verbatim}
            P(a,g) :- R(a,b), b<=g, S(g).
            ?- P(x,y)
        \end{verbatim}
        Another solution would be to stablish restrictions with respect to the variables that can participate in these variable-against-variable
        comparisons.
    \item \textbf{Arithmetic expressions}: YADI does not allow any type of arithmetic expressions, it would
        be useful to allow comparisons such as ``x $<$ 2*y'' in a rule's body, or arithmetic expressions in rule's
        head such as ``P(2*x,y) :- ...''.
    \item \textbf{IDB column names}: in its current state, YADI names IDB table columns as ``col0'', ``col1'', and so on.
        This makes it somewhat hard to interpret a resulting table. This can be solved by inferring the name of the columns out
        of the names of EDB tables or variable names used in their definition.
    \item \textbf{Line information in error reporting}: when reporting errors, it would be useful to include the line and column
        in the program where the error was found. This can be done by augmenting the AST data structure for including this
        information.
\end{itemize}

\end{document}
